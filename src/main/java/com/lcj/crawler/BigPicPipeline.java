package com.lcj.crawler;

import com.geccocrawler.gecco.pipeline.Pipeline;
import com.lcj.exec.DownloadAction;
import com.lcj.exec.Executors;
import com.lcj.model.Picture;
import com.lcj.service.PictureService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2017/4/9/009.
 */
@Service
public class BigPicPipeline implements Pipeline<BigPic> {

    @Resource(name = "pictureServiceImpl")
    private PictureService pictureService;

    @Value("${local.path}")
    private String fileSavePath;

    private Executors executors = Executors.create();

    @Override
    public void process(BigPic bean) {
        Picture pic = new Picture();
        pic.setPicinfo(bean.getCode());

        for (String url : bean.getPics()) {
            pic.setUrl(url);
            String localPath = fileSavePath + System.currentTimeMillis() + ".jpg";
            pic.setPath(localPath);
            pictureService.save(pic);
            executors.getDefaultActionQueue().enqueue(
                    new DownloadAction(executors.getDefaultActionQueue(), url, localPath));
        }
    }
}
