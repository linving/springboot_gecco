package com.lcj.crawler;

import com.geccocrawler.gecco.GeccoEngine;
import com.geccocrawler.gecco.annotation.Gecco;
import com.geccocrawler.gecco.annotation.HtmlField;
import com.geccocrawler.gecco.annotation.Request;
import com.geccocrawler.gecco.request.HttpGetRequest;
import com.geccocrawler.gecco.request.HttpRequest;
import com.geccocrawler.gecco.spider.HtmlBean;
import com.geccocrawler.gecco.spider.SpiderBean;
import com.lcj.model.CategoryType;

import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 * 1,抓取开始
 */
@Gecco(matchUrl = "http://www.meizitu.com/", pipelines = { "consolePipeline", "saveCategoryPipeline" })
public class IndexPage implements HtmlBean, SpiderBean {

    private static final long serialVersionUID = -139677387757121011L;

    @Request
    private HttpRequest request;

    @HtmlField(cssPath = ".topmodel > ul > li")
    private List<CategoryType> categoryTypes;

    public List<CategoryType> getCategoryTypes() {
        return categoryTypes;
    }

    public void setCategoryTypes(List<CategoryType> categoryTypes) {
        this.categoryTypes = categoryTypes;
    }

    public HttpRequest getRequest() {
        return request;
    }

    public void setRequest(HttpRequest request) {
        this.request = request;
    }

    public static void main(String[] args) {
        HttpGetRequest start = new HttpGetRequest("http://www.meizitu.com/");
        start.setCharset("GBK");
        GeccoEngine.create()
                .classpath("com.lcj")
                .start(start)
                .run();
    }

}