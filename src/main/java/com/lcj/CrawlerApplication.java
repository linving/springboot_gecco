package com.lcj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class CrawlerApplication {

    public static void main(String[] args) throws Exception {
        ApplicationContext context = SpringApplication.run(CrawlerApplication.class, args);
        /*在配置中实例化*/
        /*PipelineFactory springPipelineFactory = (PipelineFactory)context.getBean("springPipelineFactory");
        HttpGetRequest start = new HttpGetRequest("http://www.meizitu.com/");
        start.setCharset("GBK");
        GeccoEngine.create()
                .classpath("com.lcj")
                .pipelineFactory(springPipelineFactory)
                .interval(2000)
                .start(start)
                .run();*/
    }
}