package com.lcj.service.impl;

import com.lcj.dao.PictureInfoDao;
import com.lcj.model.PictureInfo;
import com.lcj.service.PictureInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Administrator on 2017/4/9/009.
 */
@Service
public class PictureInfoServiceImpl implements PictureInfoService {

    @Autowired
    private PictureInfoDao pictureInfoDao;

    @Override
    public int save(PictureInfo pictureInfo) {

        return pictureInfoDao.insert(pictureInfo);
    }

}
