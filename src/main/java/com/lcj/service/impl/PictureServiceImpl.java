package com.lcj.service.impl;

import com.lcj.dao.PictureDao;
import com.lcj.model.Picture;
import com.lcj.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 */
@Service
public class PictureServiceImpl implements PictureService {

    @Autowired
    private PictureDao pictureDao;

    @Override
    public int save(Picture picture) {
        return pictureDao.insert(picture);
    }

    @Override
    public List<Picture> selectPages(int start, int limit) {
        return pictureDao.selectPages(start,limit);
    }

}
