package com.lcj.service;

import com.lcj.model.Picture;

import java.util.List;

/**
 * Created by Administrator on 2017/4/9/009.
 */
public interface PictureService {
    int save(Picture picture);

    List<Picture> selectPages(int start, int limit);
}
