package com.lcj.dao;

import com.lcj.model.PictureInfo;
import org.apache.ibatis.annotations.Insert;
import org.springframework.stereotype.Repository;

/**
 * Created by Administrator on 2017/4/9/009.
 */
@Repository
public interface PictureInfoDao {

    @Insert("INSERT INTO `t_mz_picinfo` (`tag_code`, `alt`, `thumbnail`, `href`, `create_time`, `update_time`,tag_name,pic_code) VALUES (#{tagCode}, #{alt}, #{thumbnail}, #{href}, NOW(), NOW(),#{tagName},#{picCode})")
    int insert(PictureInfo record);
}
