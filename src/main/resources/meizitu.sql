/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50717
Source Host           : localhost:3306
Source Database       : meizitu

Target Server Type    : MYSQL
Target Server Version : 50717
File Encoding         : 65001

Date: 2017-04-09 23:08:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_mz_pic
-- ----------------------------
DROP TABLE IF EXISTS `t_mz_pic`;
CREATE TABLE `t_mz_pic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `picinfo` int(11) DEFAULT NULL COMMENT '图片信息',
  `url` varchar(100) NOT NULL DEFAULT '0' COMMENT '图片地址',
  `path` varchar(100) NOT NULL DEFAULT '0' COMMENT '下载下来后的本地地址(保存相对路径)',
  `create_time` datetime DEFAULT '0000-00-00 00:00:00' COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1107 DEFAULT CHARSET=utf8 COMMENT='图片地址(原始地址，以及本地地址)';

-- ----------------------------
-- Table structure for t_mz_picinfo
-- ----------------------------
DROP TABLE IF EXISTS `t_mz_picinfo`;
CREATE TABLE `t_mz_picinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_code` varchar(11) NOT NULL DEFAULT '0' COMMENT '所属标签',
  `alt` varchar(255) NOT NULL DEFAULT '0' COMMENT '图片描述',
  `thumbnail` varchar(255) NOT NULL DEFAULT '0' COMMENT '缩略图',
  `href` varchar(255) NOT NULL DEFAULT '0' COMMENT '超链接',
  `create_time` datetime DEFAULT '0000-00-00 00:00:00',
  `update_time` datetime DEFAULT '0000-00-00 00:00:00',
  `tag_name` varchar(255) DEFAULT NULL COMMENT '标签名字',
  `pic_code` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=331 DEFAULT CHARSET=utf8 COMMENT='图片信息';

-- ----------------------------
-- Table structure for t_mz_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_mz_tag`;
CREATE TABLE `t_mz_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(50) DEFAULT '0' COMMENT '分类',
  `url` varchar(100) DEFAULT NULL COMMENT '链接',
  `tag_pinyin` varchar(255) DEFAULT NULL COMMENT '标签拼音',
  `tag_code` varchar(11) DEFAULT NULL COMMENT '标签编码',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='妹子图-分类';
